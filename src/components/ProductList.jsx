import React, { Fragment } from "react";
import Title from "./Title";
import Product from "./Product";
import { ProductConsumer } from "../context";

const ProductList = () => {
  return (
    <Fragment>
      <div className="py-5">
        <div className="container">
          <Title name="محصولات" title="ما" />
          <div className="row">
            <ProductConsumer>
              {value =>
                value.products.map(product => {
                  return <Product key={product.id} product={product} />;
                })
              }
            </ProductConsumer>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
export default ProductList;
