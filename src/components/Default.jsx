import React from "react";
import styled from "styled-components";
import Notlogo from "../notlogo.png";

const Default = ({ location }) => {
  return (
    <NotFoundDiv className="container">
      <div className="row">
        <img
          style={{ height: "130px" }}
          src={Notlogo}
          className="Not-logo d-none d-lg-block"
          alt="Page Not Found"
        />
        <h4 className="text-wrap mt-5 mx-5 text-center">
          the Requested URL{" "}
          <span className="text-danger">{location.pathname}</span> Was Not Found
          !
        </h4>
        <img
          style={{ height: "130px" }}
          src={Notlogo}
          className="Not-logo d-none d-lg-block"
          alt="Page Not Found"
        />
      </div>
    </NotFoundDiv>
  );
};

const NotFoundDiv = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  margin-top: 25%;
`;

export default Default;
