import React from "react";
import styled from "styled-components";
import { ProductConsumer } from "../context";
import { ButtonContainer } from "./Button";
import { Link } from "react-router-dom";

const Modal = () => {
  return (
    <ProductConsumer>
      {value => {
        const { modalOpen, closeModal } = value;
        const { img, price, title } = value.modalProduct;

        if (!modalOpen) {
          return null;
        } else {
          return (
            <ModalContainer>
              <div className="container">
                <div className="row">
                  <div
                    id="modal"
                    className="col-8 mx-auto col-md-6 col-lg-4 text-center text-capitalize p-5"
                  >
                    <h5>محصول به سبد اضافه شد</h5>
                    <img src={img} className="img-fluid" alt="Product" />
                    <h5>{title}</h5>
                    <p className="text-muted">
                      قیمت :{" "}
                      <span style={{ fontFamily: "Ghasem" }}>{price} </span>{" "}
                      تومان
                    </p>
                    <Link to="/">
                      <ButtonContainer
                        cart
                        className="mx-2"
                        onClick={() => closeModal()}
                      >
                        ادامه خرید
                      </ButtonContainer>
                    </Link>
                    <Link to="/cart">
                      <ButtonContainer cart onClick={() => closeModal()}>
                        سبد خرید
                      </ButtonContainer>
                    </Link>
                  </div>
                </div>
              </div>
            </ModalContainer>
          );
        }
      }}
    </ProductConsumer>
  );
};

const ModalContainer = styled.div`
  position: fixed;
  transition: 2s;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background: rgba(0, 0, 0, 0.3);
  display: flex;
  align-items: center;
  justify-content: center;
  #modal {
    background: #34323b;
  }
`;
export default Modal;
