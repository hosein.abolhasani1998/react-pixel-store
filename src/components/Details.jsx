import React, { Component } from "react";
import { ProductConsumer } from "../context";
import { ButtonContainer } from "./Button";
import { Link } from "react-router-dom";
class Details extends Component {
  render() {
    return (
      <ProductConsumer>
        {value => {
          const {
            id,
            title,
            price,
            info,
            inCart,
            img,
            company
          } = value.detailProduct;
          return (
            <div className="container py-5">
              <div className="row">
                <div className="col-10 mx-auto text-center text-red my-5 ">
                  <h3>{title}</h3>
                </div>
              </div>
              <div className="row">
                <div className="col-10 mx-auto col-md-6 my-3">
                  <img src={img} className="img-fluid" alt="Product" />
                </div>
                <div className="col-10 mx-auto col-md-6 my-3 text-right">
                  <h5>مدل : {title}</h5>
                  <h6 className="text-title text-uppercase text-muted mt-3 mb-2">
                    محصول : <span className="text-uppercase">{company}</span>
                  </h6>
                  <h6 className="text-red">
                    <strong style={{ fontFamily: "Ghasem" }}>
                      قیمت : {price} <span>تومان</span>
                    </strong>
                  </h6>
                  <p className=" mt-3 mb-0">
                    اطلاعاتی درمورد محصول :
                    <p
                      style={{ fontSize: "1rem", lineHeight: "2rem" }}
                      className="text-muted lead"
                    >
                      {info}
                    </p>
                  </p>
                  <div>
                    <Link to="/">
                      <ButtonContainer cart>بازگشت به محصولات</ButtonContainer>
                    </Link>

                    <ButtonContainer
                      className="mr-3"
                      cart
                      disabled={inCart ? true : false}
                      onClick={() => {
                        value.addToCart(id);
                        value.openModal(id);
                      }}
                    >
                      {inCart ? "در سبد موجود است" : "اضافه کردن به سبد"}
                    </ButtonContainer>
                  </div>
                </div>
              </div>
            </div>
          );
        }}
      </ProductConsumer>
    );
  }
}
export default Details;
