import React, { Fragment } from "react";
import { Link } from "react-router-dom";

const CartTotals = ({ value }) => {
  const { cartSubTotal, cartTax, cartTotal, clearCart } = value;
  return (
    <Fragment>
      <div className="container">
        <div className="row">
          <div className="col-12 mt-5   text-center">
            <Link to="/">
              <button
                className="btn btn-outline-danger  mb-3 px-5"
                type="button"
                onClick={() => clearCart()}
              >
                پاک کردن سبد خرید
              </button>
            </Link>
            <h5>
              <span>
                قیمت خام :{" "}
                <span style={{ fontFamily: "Ghasem" }}>{cartSubTotal}</span>{" "}
                تومان{" "}
              </span>
            </h5>
            <h5>
              <span>
                مالیات بر ارزش افزود :{" "}
                <span style={{ fontFamily: "Ghasem" }}>{cartTax}</span> تومان
              </span>
            </h5>
            <h5>
              <span>
                مبلغ قابل پرداخت :{" "}
                <span style={{ fontFamily: "Ghasem" }}>{cartTotal}</span> تومان{" "}
              </span>
            </h5>
          </div>
        </div>
      </div>
    </Fragment>
  );
};
export default CartTotals;
