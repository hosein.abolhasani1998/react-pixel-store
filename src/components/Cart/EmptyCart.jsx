import React from "react";

const EmptyCart = () => {
  return (
    <div className="container mt-5">
      <div className="row">
        <div className="col-10 mx-auto text-center ">
          <h2 className="text-white">
            در حال حاضر <span className="text-red">سبد خرید</span> شما خالـی است
            !
          </h2>
        </div>
      </div>
    </div>
  );
};
export default EmptyCart;
