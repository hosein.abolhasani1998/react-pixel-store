import React from "react";

const CartItem = ({ item, value }) => {
  const { id, title, img, price, total, count } = item;
  const { increment, decrement, removeItem } = value;
  return (
    <div className="row my-2 text-center">
      <div className="col-12  mx-auto col-lg-2 my-4 my-lg-0">
        <img
          src={img}
          className="img-fluid "
          alt="product"
          style={{ width: "5rem", height: "5rem" }}
        />
      </div>
      <div className="col-10 mx-auto col-lg-2">
        <span className="d-lg-none">محصول :</span> {title}
      </div>

      <div className="col-10 mx-auto col-lg-2">
        <span className="d-lg-none">قیمت :</span>{" "}
        <span
          className="font-weight-bold"
          style={{ fontFamily: "Ghasem", letterSpacing: ".2rem" }}
        >
          {price}
        </span>
      </div>
      <div className="col-10 mx-auto col-lg-2 my-2 my-lg-0">
        <div className="d-flex justify-content-center">
          <div>
            <span
              style={{ background: "#5A5665" }}
              className="btn mx-1"
              onClick={() => decrement(id)}
            >
              -
            </span>
            <span
              style={{ background: "#5A5665", fontFamily: "Ghasem" }}
              className="btn mx-1"
            >
              {count}
            </span>
            <span
              style={{ background: "#5A5665" }}
              className="btn  mx-1"
              onClick={() => increment(id)}
            >
              +
            </span>
          </div>
        </div>
      </div>
      <div className="col-10 mx-auto col-lg-2">
        <div className="cart-icon" onClick={() => removeItem(id)}>
          <i className="fas fa-trash" />
        </div>
      </div>
      <div className="col-10 mx-auto col-lg-2">
        <strong>
          جمع قیمت :<span style={{ fontFamily: "Ghasem" }}>{total}</span> تومان
        </strong>
      </div>
    </div>
  );
};
export default CartItem;
