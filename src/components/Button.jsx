import styled from "styled-components";

export const ButtonContainer = styled.button`
  text-transform: capitalize;
  background: tomato;
  background: ${props => (props.cart ? "#F63954" : "var( --mainYellow)")};
  border-radius: 3px;
  border: none;
  color: white;
  padding: 0.4rem 0.6rem;
  transition: all 0.35s ease-out;
  &:hover {
    background: var(--lightBlue);
    background: ${props => (props.cart ? "var( --maindark)" : "#F63954")};
  }
`;
