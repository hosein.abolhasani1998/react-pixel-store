import React from "react";
import { Link } from "react-router-dom";
import logo from "../logo.png";
import { ButtonContainer } from "./Button";
const Navbar = () => {
  return (
    <nav className="navbar navbar-expand-sm bg-gray  navbar-dark px-sm-5">
      <Link to="/">
        <img src={logo} alt="store" className="navbar-brand" />
      </Link>
      <ul className="navbar-nav align-items-center">
        <li className="nav-item  ml-1">
          <Link to="/" className="nav-link  text-white">
            <span className="product-item"> محصولات</span>
          </Link>
        </li>
      </ul>
      <Link to="/cart" className="mr-auto">
        <ButtonContainer>
          <i className="fas fa-cart-plus" /> سبد خـرید
        </ButtonContainer>
      </Link>
    </nav>
  );
};

export default Navbar;
