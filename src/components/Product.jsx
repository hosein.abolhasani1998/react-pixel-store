import React from "react";
import { ProductConsumer } from "../context";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";

const Product = props => {
  const { id, title, img, price, inCart } = props.product;

  return (
    <div className="col-9 mx-auto col-md-6 col-lg-3 my-3">
      <div className="cardi ">
        <ProductConsumer>
          {value => (
            <div
              className="img-container p-5"
              onClick={() => {
                value.handleDetail(id);
              }}
            >
              <Link to="/details">
                <img src={img} alt="Product" className="card-img-top " />
              </Link>
              <button
                className="cart-btn"
                disabled={inCart ? true : false}
                onClick={() => {
                  value.addToCart(id);
                  value.openModal(id);
                }}
              >
                {inCart ? (
                  <p
                    style={{ fontSize: ".85rem" }}
                    className="text-capitalize mb-0"
                    disabled
                  >
                    در سبد موجود است
                  </p>
                ) : (
                  <i className="fas fa-cart-plus" />
                )}
              </button>
            </div>
          )}
        </ProductConsumer>
        <div className="card-footer d-flex justify-content-between bg-dark border-0">
          <p style={{ fontSize: ".75rem" }} className="align-self-center mb-0 ">
            {title}
          </p>
          <p
            style={{ fontSize: ".85rem", fontFamily: "Ghasem" }}
            className="text-red font-weight-bold  mb-0"
          >
            {price} <span>تومان</span>
          </p>
        </div>
      </div>
    </div>
  );
};

Product.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number,
    img: PropTypes.string,
    title: PropTypes.string,
    price: PropTypes.number,
    inCart: PropTypes.bool
  }).isRequired
};

export default Product;
